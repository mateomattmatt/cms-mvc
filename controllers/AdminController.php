<?php

class AdminController extends Controller {
  public function __construct () {
    parent::__construct();
    parent::middleware('auth|initial');
  }

  public function index() {
    $this->view->redirect('admin/home');
  }

  public function home () { 
    $images = DB_Table::all('images');

    $home = [
      'home_hero',
      'about_title',
      'about_body',
      'gallery_title',
      'gallery_img',
      'contact_title',
      'contact_body',
      'contact_map'
    ];


    for ($i = 0; $i < count($home); $i++) {
      $item = DB_Table::where('items', ["code" => $home[$i]])[0];
      switch ($item->code) {
        case 'home_hero':
          $this->view->home_hero = $item->value;
          break;
        case 'about_title':
          $this->view->about_title = $item->value;
          break;
        case 'about_body':
          $this->view->about_body = $item->value;
          break;
        case 'gallery_title':
          $this->view->gallery_title = $item->value;
          break;
        case 'gallery_img':
          $this->view->gallery_img = json_decode($item->value);
          break;
        case 'contact_title':
          $this->view->contact_title = $item->value;
          break;
        case 'contact_body':
          $this->view->contact_body = $item->value;
          break;
        case 'contact_map':
          $this->view->contact_map = $item->value;
          break;
      }
    }
    
    $this->view->images = $images;
    $this->view->render('admin/home');
  }

  public function leads () {
    $this->view->leads = DB_Table::all('leads');
    $this->view->render('admin/leads');
  }

  public function users () {
    $this->view->users = DB_Table::all('users');
    $this->view->render('admin/users');
  }
  
  public function profile () {
    Session::init();
    $this->view->user = Session::get('user');
    $this->view->render('admin/profile');
  }

  public function profileupdate() {
    Session::init();
    $user_id = DB_Table::update('users', [
      "fname" 	=> $_POST['fname'],
      "lname" 	=> $_POST['lname'],
      "email" 	=> $_POST['email'],
    ], ["id" => Session::get('user')->id]);

    if($user_id){
      $user = DB_Table::where("users", ["id" => Session::get('user')->id])[0];
      Session::set('user', null);
      Session::set('user', $user);

    }
    header('Location: '.SITE_URL.'admin/profile');
      
  }

  public function profilechangepass() {
    Session::init();
		if(!empty($_POST['old_password']) && !empty($_POST['password']) && !empty($_POST['cpassword']) ){
			$r = Validation::validate([
				$_POST['password'] => "confirm | ".$_POST['cpassword']
			]);
			if(password_verify($_POST['old_password'], Session::get('user')->password) && $r){
				$id = DB_Table::update("users", [
					"password" 	=> password_hash($_POST['password'], PASSWORD_DEFAULT)
				], ["id" => Session::get("user")->id]);
				header('location: '.SITE_URL.'admin/profile');
			}
			else{
				if(!$r){
					Session::set('result_msg', "Wrong old password.");
					header('location: '.SITE_URL.'admin/profile');
				}
				else{
					Session::set('result_msg', "Password didn't match");
					header('location: '.SITE_URL.'admin/profile');
				}
			}
		} else{
			Session::set('result_msg', "Please Input Information");
			header('location: '.SITE_URL.'admin/profile');
		}
	}

  public function userstore () {
    if ($this->view->post()) {
      Session::init();
      $validate = Validation::validate([
				$_POST['fname'] => "letters | required",
				$_POST['lname'] => "letters | required",
				$_POST['email'] => "email | unique | required",	
				$_POST['password'] => "confirm | ".$_POST['cpassword']
      ]);
      
      if ($validate) {
        $user_id = DB_Table::insert('users', [
					"fname" 	=> $_POST['fname'],
					"lname" 	=> $_POST['lname'],
					"email" 	=> $_POST['email'],
					"password" 	=> password_hash($_POST['password'], PASSWORD_DEFAULT)
        ]);
        
        if ($user_id) {
          $user = DB_Table::where('users', ['id' => $user_id]);
          if (count($user) == 1) {
            Session::set('user', $user[0]);
            $this->view->redirect('admin/users');
            exit;
          } else {
            $this->view->error();
          }
        } else {
          Session::set('error', "Sorry for what happened, currently we can't saved data in the storage.");
          $this->view->redirect('admin/users');
        }
      } else {
        Session::set('error', "Please fix your inputs.");
        $this->view->redirect('admin/users');
      }
    } else {
      $this->view->error();
    }

  }

  public function update() {
    $home = [
      'home_hero',
      'about_title',
      'about_body',
      'gallery_title',
      'gallery_img',
      'contact_title',
      'contact_body',
      'contact_map'
    ];
    for ($i = 0; $i < count($home); $i++) {
      $item = DB_Table::where('items', ["code" => $home[$i]])[0];

      $value = $_POST[$home[$i]];
      if ($home[$i] == 'gallery_img') {
        $value = json_encode($_POST[$home[$i]]);
      }

      if ($item) {
        DB_Table::update('items', [
          "value" 	=> $value,
        ], ["id" => $item->id]);
      } else {
        $item_id = DB_Table::insert('items', [
            "code"          => $home[$i],
            "value"         => $value
        ]);
        echo $item_id;
      }
    }
    $this->view->redirect('admin/home');
  }
}

