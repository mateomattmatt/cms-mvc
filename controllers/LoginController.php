<?php 

class LoginController extends Controller{
	public function __construct () {
        parent::__construct();
        parent::middleware('guest|initial');
	}

    public function index() {
        $this->view->render('auth/login');
    }
}