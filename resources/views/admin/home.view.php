<div id="wrapper">
  <?php include 'resources/components/sidebar.view.php'; ?>
  <div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
      <?php include 'resources/components/topbar.view.php'; ?>

      <form method="POST" action="/admin/update">
        <div class="container-fluid">
          <div class="row">
            <div class="col-xl-8 col-lg-7">
              <div class="card shadow mb-4">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold">Homepage Content</h6>
                </div>
                <div class="card-body">
                  <label class="font-weight-bold" for="">Home Banner</label>
                  <div class="form-group">
                    <label for="">Hero Image</label>
                    <input type="text" name="home_hero" class="form-control" placeholder="Please add the link of the hero image here..." value="<?=$this->home_hero?>" />
                    <div class="text-right py-2">
                      <button type="button" class="btn btn-sm btn-secondary btn-media" data-toggle="modal" data-target="#media-gallery">Media Gallery</button>
                    </div>
                  </div>
                </div>
                <hr />
                <div class="card-body">
                  <label for="" class="font-weight-bold">About Section</label>
                  <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" name="about_title" class="form-control" placeholder="Please add section title here..." value="<?=$this->about_title?>" />
                  </div>
                  <div class="form-group">
                    <label for="">Body</label>
                    <div id="summernote"></div>
                    <textarea id="summer" class="summernote" name="about_body">
                      <?=$this->about_body?>
                    </textarea>
                  </div>
                </div>
                <hr />
                <div class="card-body">
                  <label class="font-weight-bold">Gallery Section</label>
                  <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" name="gallery_title" class="form-control" placeholder="Please add gallery title here..." value="<?=$this->gallery_title?>" />
                  </div>
                  <hr>
                  <div class="form-group">
                    <div class="d-flex justify-content-between ">
                      <label for="">List of images</label>
                      <button type="button" class="btn btn-sm btn-secondary btn-media" data-toggle="modal" data-target="#media-gallery">Media Gallery</button>
                    </div>
                    <hr>
                    <div class="additional-images">
                      <?php for($i = 0; $i < count($this->gallery_img); $i++): ?>
                        <input type="text" class="form-control mb-2" name="gallery_img[]" value="<?=$this->gallery_img[$i]?>" placeholder="Please add new image here..." />
                      <?php endfor; ?>
                    </div>
                    <div class="additional-button text-right">
                      <button class="btn btn-sm btn-primary" type="button">
                        <i class="fa fa-plus"></i> Add New Image
                      </button>
                    </div>
                  </div>
                </div>
                <hr />
                <div class="card-body">
                  <label for="" class="font-weight-bold">Contact Section</label>
                  <div class="form-group">
                    <label for="">Title</label>
                    <input type="text" name="contact_title" class="form-control" value="<?=$this->contact_title?>" placeholder="Please add contact title here..." />
                  </div>
                  <div class="form-group">
                    <label for="">Body</label>
                    <div id="contactnote"></div>
                    <textarea id="contactsummer" class="summernote" name="contact_body">
                      <?=$this->contact_body?>
                    </textarea>
                  </div>
                  <div class="form-group">
                    <label for="">Embedded Map</label>
                    <textarea name="contact_map" class="form-control" id="" rows="5">
                      <?=$this->contact_map?>
                    </textarea>
                  </div>
                  <!-- End of Form -->
                </div>
              </div>
              <!-- End of home -->
            </div>
            <div class="col-xl-4 col-lg-5">
              <div class="card shadow mb-4 sticky">
                <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                  <h6 class="m-0 font-weight-bold">General Settings</h6>
                </div>
                <div class="card-body">
                  <p>Last Updated: </p>
                  <p>Version: </p>
                  <div class="d-flex justify-content-end">
                    <button class="btn btn-sm btn-primary shadow-sm">Save Settings</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
      
    </div>
  </div>
</div>

<?php include 'resources/components/images.view.php'; ?>

<script>
  $('#summernote').summernote({
    placeholder: 'Insert content here..',
    tabsize: 2,
    height: 300,
    callbacks: {
      onChange: function(contents, $editable) {
        $('#summer').val(contents);
      }
    }
  });
  $('#summernote').summernote('code', $('#summer').val());

  $('#contactnote').summernote({
    placeholder: 'Insert content here..',
    tabsize: 2,
    height: 300,
    callbacks: {
      onChange: function(contents, $editable) {
        $('#contactsummer').val(contents);
      }
    }
  });
  $('#contactnote').summernote('code', $('#contactsummer').val());

  $('.additional-button button').on('click', function () {
    $('.additional-images').append(`
      <input type="text" class="form-control mb-2" name="gallery_img[]" placeholder="Please add new image here..." />
    `);
  });

  if (window.location.href.indexOf('#mediaGallery') > 0) {
    $('#media-gallery').modal('show');
  }
</script>

