<!-- Import Scripts here -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js"></script>
<script src="../node_modules/jquery/dist/jquery.min.js"></script>
<script src="../node_modules/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="../node_modules/slick-carousel/slick/slick.min.js"></script>
<script src="../public/js/sb-admin-2.min.js"></script>
<script src="../public/js/isotope.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-bs4.js"></script>
<script>
function masonry() {
    $('.masonry').isotope({
        itemSelector: '.masonry-item',
        layoutMode: 'masonry',
        masonry: {
            columnWidth: '.masonry-sizer'
        },
    })
}
$(document).ready(function() {
    masonry();
})
</script>