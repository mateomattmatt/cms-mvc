<?php 

class Bootstrap{
	public function __construct(){
		$url = isset($_GET['url']) ? $_GET['url'] : null;
		$url = rtrim($url, '/');
		$url = explode('/', $url);

		$ctrlName = ucfirst($url[0]);

		if(empty($url[0])){
			require 'controllers/IndexController.php';
			$controller = new IndexController();
			$controller->index();
			return false;
		}

		$file = 'controllers/'.$ctrlName.'Controller.php';

		if(file_exists($file)){
			require $file;	
		}
		else{
			require 'controllers/ErrorController.php';
			$controller = new ErrorController();
			$controller->error404();
			return false;
		}
		$className = $ctrlName.'Controller';

		$controller = new $className;

		if(isset($url[3])){
			$params = array();

			for ($i=2; $i < count($url); $i++) { 
				array_push($params, $url[$i]);
			}

			call_user_func_array(array($controller, $url[1]), $params);
		}
		else{
			if(isset($url[2])){
				if(method_exists($controller, $url[1])){
					$controller->{$url[1]}($url[2]);
				}
				else{
					$this->error();
				}
			}
			else{
				if(isset($url[1])){
					if(method_exists($controller, $url[1])){
						$controller->{$url[1]}();
					}else{
						$this->error();		
					}	
				}	
				else{
					$controller->index();		
				}
			}
		}
	}

	public function error(){
		require 'controllers/ErrorController.php';
		$controller = new ErrorController();
		$controller->error404();
		return false;
	}
}