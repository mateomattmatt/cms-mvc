<?php 

class View {
  public function __construct(){}

  public function render($name, $noInclude = false) {
    if ($noInclude === true) {
      require 'resources/views/'.$name.'.view.php';
    } else {
      require 'resources/layout/header.view.php';
      require 'resources/styles.php';
      require 'resources/scripts.php';
      require 'resources/views/'.$name.'.view.php';
      require 'resources/layout/footer.view.php';
    }
  }

  public function redirect($path) {
	header('Location: '.SITE_URL.$path);
  }

  public function error() {
	$this->render('error/index');
  }

  public function post() {
	return $_SERVER['REQUEST_METHOD'] == 'POST';
  }
  public function get() {
	return $_SERVER['REQUEST_METHOD'] == 'GET';
  }
  public function put() {
	return $_SERVER['REQUEST_METHOD'] == 'PUT';
  }
  public function delete() {
	return $_SERVER['REQUEST_METHOD'] == 'DELETE';
  }


}