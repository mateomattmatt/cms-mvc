<?php 

spl_autoload_register(function($class_name){
	require 'libs/'.$class_name.'.php';
});

require 'utils/config.php';

$app = new Bootstrap();