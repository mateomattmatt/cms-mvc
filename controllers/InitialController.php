<?php

class InitialController extends Controller {
  public function __construct () {
    parent::__construct();
    parent::middleware('check-super-admin');
  }

  public function index () { 
    $this->view->render('auth/register');
  }

  public function register () {
    if ($this->view->post()) {
      Session::init();
      $validate = Validation::validate([
				$_POST['fname'] => "letters | required",
				$_POST['lname'] => "letters | required",
				$_POST['email'] => "email | unique | required",	
				$_POST['password'] => "confirm | ".$_POST['cpassword']
      ]);
      
      if ($validate) {
        $user_id = DB_Table::insert('users', [
					"fname" 	=> $_POST['fname'],
					"lname" 	=> $_POST['lname'],
					"email" 	=> $_POST['email'],
					"password" 	=> password_hash($_POST['password'], PASSWORD_DEFAULT)
        ]);
        
        if ($user_id) {
          $user = DB_Table::where('users', ['id' => $user_id]);
          if (count($user) == 1) {
            Session::set('user', $user[0]);
            $this->view->redirect('admin/home');
            exit;
          } else {
            $this->view->error();
          }
        } else {
          Session::set('error', "Sorry for what happened, currently we can't saved data in the storage.");
          $this->view->redirect('login');
        }
      } else {
        Session::set('error', "Please fix your inputs.");
        $this->view->redirect('login');
      }
    } else {
      $this->view->error();
    }

  }
}

