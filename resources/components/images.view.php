<div class="modal" tabindex="-1" role="dialog" id="media-gallery">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Media Gallery</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form action="/images/add" method="POST" enctype="multipart/form-data">
        <label for="">Add new image</label>
          <div class="form-group input-group">
            <input type="file" name="image" class="form-control"/>
            <button class="btn btn-sm btn-primary" type="submit">Upload</button>
          </div>
        </form>
        <hr />
        <div class="row masonry">
          <div class="col-6 col-md-4 col-lg-3 masonry-sizer"></div>
          <?php for($i = 0; $i < count($this->images); $i++):
            $image = $this->images[$i];
            ?>

          <div class="col-6 col-md-4 col-lg-3 masonry-item">
            <div class="images-container">
              <img src="<?=$image->path?>" alt="<?=$image->name?>">
              <p><?=$image->path?></p>
            </div>
          </div>
          <?php endfor; ?>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal-close">Close</button>
      </div>
    </div>
  </div>
</div>

<script>

function masonry() {
    $('.masonry').isotope({
        itemSelector: '.masonry-item',
        layoutMode: 'masonry',
        masonry: {
            columnWidth: '.masonry-sizer'
        },
    })
}
  $('#modal-close').on('click', function() {
    window.history.pushState({}, document.title, "/admin/home");
  }); 
  $(".btn-media").on('click', function() {
    window.history.pushState({}, document.title, "/admin/home#mediaGallery");
    setTimeout(() => {
      masonry();
      
    }, 200);
  })
</script>