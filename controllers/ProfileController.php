<?php
class ProfileController extends Controller{
	public function __construct(){
		parent::__construct();
	}
	public function index(){
		Session::init();
		if(Session::get('user')){
			$this->view->render('users/profile');
		}
		else{
			header("location: ".SITE_URL);
		}
	}
	public function edit(){
		Session::init();
		if(Session::get('user')){
			$this->view->user = DB_Table::where('users', ["id" => Session::get('user')->id])[0];
			$this->view->render('users/edit');
		}
		else{
			header("location: ".SITE_URL);
		}
	}
	public function changePassword(){
		Session::init();
		if(Session::get('user')){
			$this->view->render('users/changePassword');
		}
		else{
			header("location: ".SITE_URL);
		}
	}
	public function changepass(){
		Session::init();
		if(!empty($_POST['old_pass']) && !empty($_POST['password']) && !empty($_POST['cpassword']) ){
			$r = Validation::validate([
				$_POST['password'] => "confirm | ".$_POST['cpassword']
			]);
			if(password_verify($_POST['old_pass'], Session::get('user')->password) && $r){
				$id = DB_Table::update("users", [
					"password" 	=> password_hash($_POST['password'], PASSWORD_DEFAULT)
				], ["id" => Session::get("user")->id]);
				header('location: '.SITE_URL.'profile');
			}
			else{
				if(!$r){
					Session::set('result_msg', "Wrong old password.");
					header('location: '.SITE_URL.'profile/changePassword');
				}
				else{
					Session::set('result_msg', "Password didn't match");
					header('location: '.SITE_URL.'profile/changePassword');
				}
			}
		}else{
			Session::set('result_msg', "Please Input Information");
			header('location: '.SITE_URL.'profile/changePassword');
		}
	}
	public function update(){
		Session::init();
		if(!empty($_POST['fname']) && !empty($_POST['lname']) && !empty($_POST['email'])){
			$r = Validation::validate([
				$_POST['fname'] => "letters",
				$_POST['lname'] => "letters",
				$_POST['email'] => "email | unique"
			]);
			if($r){

				if($_FILES['image']['name'] != ""){
					
				    $pathname = $_FILES['image']['name'];
					$target_dir = "public/upload/users/";
					$target_file = $target_dir . basename($_FILES["image"]["name"]);
					$image_test = DB_Table::where('users', ["id" => Session::get('users')->id])[0];
					unlink($target_dir.$image_test->image);

					$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

					$extensions_arr = array("jpg","jpeg","png","gif");

					$temp = $imageFileType;
					if(in_array($imageFileType,$extensions_arr)) {
						move_uploaded_file($_FILES['image']['tmp_name'],$target_dir.$pathname);
					}

					rename($target_dir.$pathname, $target_dir.Session::get("user")->id.".".$temp);			
				}
				$user_id = DB_Table::update('users', [
					"fname" 	=> $_POST['fname'],
					"lname" 	=> $_POST['lname'],
					"email" 	=> $_POST['email'],
					"address" 	=> $_POST['address']
				], ["id" => Session::get('user')->id]);

				if($user_id){
					$user = DB_Table::where("users", ["id" => Session::get('user')->id])[0];
					Session::set('user', null);
					Session::set('user', $user);

					header('Location: '.SITE_URL.'profile');
				}
					
				else{
					Session::set('result_msg', "Server Error");
					header('location: '.SITE_URL.'profile/edit');
				}
			}else{
				Session::set('result_msg', "Invalid Inputs. Please check the information and try again.");
				header('location: '.SITE_URL.'profile/edit');
			}
		}
		else{
			Session::set('result_msg', "Please fill up the forms.");
			header('location: '.SITE_URL.'profile/edit');
		}
	}
}