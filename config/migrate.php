<?php 
	include_once '../utils/config.php';
 ?>
<style type="text/css">
	body{
		padding: 1% 2%;
	}

	.row{
		width: 100%;
	}

	.form{
		width: 50%;
		margin: 0 auto;
		text-align: center;
	}

	.result{
		width: 50%;
		margin: 0 auto;
	}

	button{
		margin-top: 20px;
	}
</style>


<div class="form">
	<form action="/config/migrate.php" method="post">
		<div class="row">
			<h2>Migrations</h2>
		</div>

		<div class="row">
			Password: 
			<input type="text" name="password" required>
		</div>
		<div class="row">
			<button name="migrate">Migrate Database</button>
		</div>
	</form>	
</div>

<div class="result">
	Result:<br>
	
	<?php 
		if(isset($_POST['password'])){
			if($_POST['password'] == MIGRATION_PASS)
				include 'migrations.php';		
		}
	?>
</div>