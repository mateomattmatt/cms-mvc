# CMS_PHP


### Prerequisites

What things you need to install the framework

```
PHP => 7.0
composer => ^1.8
```


### Installation

edit variables on utils/config.php 

```php
  define('DB_TYPE', 'mysql');
  define('DB_HOST', 'localhost');
  define('DB_NAME', <DB NAME>);
  define('DB_USER', 'root');
  define('DB_PASS', null);
  define('SITE_URL', <BASE_URL>);
```

then run from terminal

```js
> composer install
```