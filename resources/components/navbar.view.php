<style>
.logo {
  width: 50px;
}
</style>

<nav class="navbar navbar-expand-lg navbar-light bg-light mb-3">
  <div class="container">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#wwf-navbar-toggle" aria-controls="wwf-navbar-toggle" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="wwf-navbar-toggle">
      <a class="navbar-brand" href="/admin">
        <img src="../../public/img/logo.png" class="logo" alt="">
      </a>
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item">
          <a class="nav-link" href="/admin">Home<span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin/about">About</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin/gallery">Gallery</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin/feedback">Feedback</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin/contact">Contact Us</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/admin/infographics">Info Graphics</a>
        </li>
      </ul>
      <ul class="navbar-nav">
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbardrop" data-toggle="dropdown">
            <?= Session::get('user')->fname ?>
          </a>
          <div class="dropdown-menu dropdown-menu-right">
            <!-- <a class="dropdown-item" href="/admin/profile">Profile</a> -->
            <!-- <div class="dropdown-divider"></div> -->
            <form action="/auth/logout" method="POST">
              <button type="submit" class="dropdown-item">Logout</button>
            </form>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>

<script>

</script>
