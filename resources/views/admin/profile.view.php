<div id="wrapper">
  <?php include 'resources/components/sidebar.view.php'; ?>
  <div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
      <?php include 'resources/components/topbar.view.php'; ?>
      <div class="container-fluid py-5">
        <div class="row">
            <div class="col-12 col-md-4 offset-md-4">
                <div class="container">
                    <div class="login-block__title">Edit Profile</div>
                    <form action="/admin/profileupdate" method="POST">
                        <div class="form-material">
                            <label for="fname">First name</label>
                            <input type="text" name="fname" id="fname" required value="<?=$this->user->fname?>"/>
                        </div>
                        <div class="form-material">
                            <label for="lname">Last name</label>
                            <input type="text" name="lname" id="lname" required value="<?=$this->user->lname?>"/>
                        </div>
                        <div class="form-material">
                            <label for="email">Email</label>
                            <input type="email" name="email" id="email" required value="<?=$this->user->email?>"/>
                        </div>
                        <div class="form-material text-right">
                            <button type="submit" class="mt-3">Update</button>
                        </div>
                    </form>
                    <hr/>
                    <div class="login-block__title">Change Password</div>
                    <form action="/admin/profilechangepass" method="POST">
                        <div class="form-material">
                            <label for="password">Old Password</label>
                            <input type="password" name="old_password" id="password" required />
                        </div>
                        <div class="form-material">
                            <label for="password">Password</label>
                            <input type="password" name="password" id="password" required />
                        </div>
                        <div class="form-material">
                            <label for="cpassword">Confirm password</label>
                            <input type="password" name="cpassword" id="cpassword" required />
                        </div>
                        <div class="form-material text-right">
                            <button type="submit" class="mt-3">Update</button>
                        </div>
                    </form>
                </div>
            </div>
            </div>

      </div>
      
    </div>
  </div>
</div>
