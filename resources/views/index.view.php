<div class="page__nav">
    <nav class="nav">
        <div class="container d-flex">
            <div class="nav__logo">
                <img src="/public/img/logo.png" alt="">
            </div>
            <div class="nav__items">
                <ul class="nav__menu -list">
                    <li class="nav__menu -item"><a href="#home">Home</a></li>
                    <li class="nav__menu -item"><a href="#about">About</a></li>
                    <li class="nav__menu -item"><a href="#gallery">Gallery</a></li>
                    <li class="nav__menu -item"><a href="#contact">Contact</a></li>
                    <li class="nav__menu -item"><a href="https://www.worldwildlife.org/ecoregions/im0130" target="_blank">WWF Tamaraw</a></li>
                </ul>
            </div>
        </div>
    </nav>
</div>

<main class="page__wrapper">
    <div class="hero-img" id="home">
        <img class="hero-img__item" src="<?=$this->home_hero?>" alt="Hero Image">
    </div>
    <div class="section container" id="about">
        <div class="section__title"><?=$this->about_title?></div>
        <div class="section__body">
            <?=$this->about_body?>
        </div>
    </div>
    <div class="section container" id="gallery">
        <div class="section__title"><?=$this->gallery_title?></div>
        <div class="section__masonry">
            <div class="masonry">
                <div class="masonry-sizer"></div>
                <?php for($i = 0; $i < count($this->gallery_img); $i++): ?>
                    <div class="masonry-item">
                        <img src="<?=$this->gallery_img[$i]?>" alt="Masonry image" />
                    </div>    
                <?php endfor; ?>
            </div>

        </div>
    </div>
    <div class="section container" id="contact">
        <div class="section__title"><?=$this->contact_title?></div>
        <div class="section__body row">
            <div class="col-12 col-md-5">
                <p>Send us a message</p>
                <form action="/leads/add" method="POST" class="section__form">
                    <div class="form-group">
                        <label for="name">Name *</label>
                        <input type="text" name="name" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone Number *</label>
                        <input type="text" name="phone" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="email">Email *</label>
                        <input type="email" name="email" class="form-control"/>
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea name="message" id="message" class="form-control" cols="30" rows="5"></textarea>
                    </div>
                    <div class="form-group text-right">
                        <button type="submit" class="btn-custom">Submit</button>
                    </div>
                </form>
            </div>
            <div class="col-md-1"></div>
            <div class="col-12 col-md-6">
                <?=$this->contact_body?>
                <div class="map">
                    <?=$this->contact_map?>
                </div>
            </div>
        </div>
    </div>
</main>


<footer class="text-center p-5">
    All Right Reserved &copy; 2019
</footer>