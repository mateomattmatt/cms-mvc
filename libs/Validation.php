<?php 

class Validation{
	public function __construct(){}

	public static function isString($str, $len = -1){
		if($len >= 0){
			if(strlen($str) > $len) return false;
		}
		else if(strlen($str)){
			return !preg_match("/[0-9]+$/", $str);	
		}
		else
			return false;
		
	}

	public static function isLetters($str, $len = -1){
		if($len >= 0){
			if(strlen($str) > $len) return false;
		}
		else if(strlen($str)){
			return preg_match("/^[a-zA-Z ]{1,191}$/", $str);	
		}
		else
			return false;
		
	}

	public static function isEmail($email, $param = false){
		if(preg_match('/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/', $email)){
			if($param == "unique"){
				$db_table = new DB_Table();
				$r = $db_table::where('users', ['email' => $email]);
				if(!empty($r)) return true;
				else return false;
			}
			else
				return true;
		}
		return false;
	}

	public static function isNumber($num, $len = -1){
		if($len >= 0){
			if(strlen($num) != $len) return false;
		}
		return preg_match("/[0-9]+$/", $num);
	}

	public static function validate($keyValue){
		$keys = array_keys($keyValue);
		$values = array_values($keyValue);

		if(count($keys) != count($values)) return false;

		for($i = 0; $i < count($keyValue); $i++){
			$nloc = strpos($values[$i], ":");
			$ploc = strpos($values[$i], "|");

			if($nloc){
				$val = preg_replace('/\s/', '', substr($values[$i], 0, $nloc));
				$param = intval(preg_replace('/\s/', '', substr($values[$i], $nloc+1)));
			}
			else if($ploc){
				$val = preg_replace('/\s/', '', substr($values[$i], 0, $ploc));
				$param = trim(substr($values[$i], $ploc+1));	
			}
			else{
				$val = $values[$i];
				$param = -1;
			}
			
			if ($val == "string") {
				if (!Validation::isString($keys[$i], $param)) return false;
			}
			else if ($val == "letters") {
				if (!Validation::isLetters($keys[$i])) return false;
			}
			else if ($val == "email") {
				if (!Validation::isEmail($keys[$i])) return false;
			}
			else if ($val == "number") {
				if (!Validation::isNumber($keys[$i], $param)) return false;
			}
			else if ($val == "confirm") {
				if (!Validation::confirm($keys[$i], $param)) return false;
			}
			else if ($val == "required") {
				if (empty($keys[$i])) return false;
			}
		}

		return true;
	}

	public static function confirm($pwd, $cpwd) {
		return ($pwd == $cpwd) ? true : false;
	}

}