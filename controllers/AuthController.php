<?php 

class AuthController extends Controller{
	public function __construct(){
		parent::__construct();
	}

	public function login() {
		Session::init();
		Session::destroyFlash('error');
		if ($this->view->post()) {
			$validate = Validation::validate([
				$_POST['email'] => "email | required",	
				$_POST['password'] => "required"
			]);
			if ($validate) {
				$email = $_POST['email'];
				$password = $_POST['password'];
				$user = DB_Table::where('users', ['email' => $email]);
	
				if (count($user) == 1) {
					if (password_verify($_POST['password'], $user[0]->password)) {
						Session::set('user', $user[0]);
						$this->view->redirect('admin/home');
					} else {
						Session::set('error', "Invalid Credentials.");	
						$this->view->redirect('login');
					}
				} else {
					Session::set('error', "User is not registered in our database.");
					$this->view->redirect('login');
				}
			} else {
				Session::set('error', "Invalid Credentials");
				$this->view->redirect('login');
			}
		} else {
			$this->view->error();
		}
	}

	public function logout() {
		if ($this->view->post()) {
			Session::init();
			Session::destroy();
			$this->view->redirect('');
		} else {
			$this->view->error();
		}
	}
}