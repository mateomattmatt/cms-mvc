<?php 

class Pagination{
	private $tableName;
	private $totalItems;
	private $perPage;
	private $currPage;

	public function __construct($tableName, $perPage, $currPage){
		$this->tableName = $tableName;
		$this->totalItems = count(DB_Table::all($tableName));
		$this->perPage = $perPage;
		$this->currPage = $currPage;
	}

	public function hasPrev(){
		if ($this->currPage > 1)
			return true;
		else
			return false;
	}

	public function hasNext(){
		if ($this->currPage < $this->getNumPages())
			return true;
		else 
			return false;
	}

	public function getNumPages(){
		$numPages = ceil($this->totalItems/$this->perPage);
		return $numPages;
	}

	public function getPageItems($orderBy = false, $trash = false){
		$db_table = new DB_Table();

		$sql = "SELECT * FROM ".$this->tableName." ";

		if (!$trash) {
			if ($db_table->checkIfColumnExists($this->tableName, 'deleted_at')) {
				$sql .= "WHERE deleted_at IS NULL ";
			}
		}

		if($orderBy != false){
			$keys = array_keys($orderBy);
			$values = array_values($orderBy);

			$sql .= "ORDER BY ";

			for($i = 0; $i < count($keys); $i++){
				$sql .= $keys[$i]." ".$values[$i]." AND ";
			}

			$sql = rtrim($sql, " AND ");
		}


		$start = ($this->currPage-1) * $this->perPage;
		$sql .= " LIMIT $start, $this->perPage";

		$result = mysqli_query($db_table->conn, $sql);

		$array = array();

		if(mysqli_num_rows($result) > 0){
			while($row = mysqli_fetch_object($result)){
				array_push($array, $row);
			}		
			return $array;
		}
		else{
			return null;
		}
		
	}

	public function getHtmlLinks($url){
		$html = '<ul class="uk-pagination uk-flex-center" uk-margin>';

		if ($this->hasPrev())
			$html .= '<li><a href="'.$url.($this->currPage-1).'"><span uk-pagination-previous></span></a></li>';
		else
			$html .= '<li class="uk-disabled"><a href="'.$url.($this->currPage-1).'"><span uk-pagination-previous></span></a></li>';


		for($i=1; $i <= $this->getNumPages(); $i++) {

			if ($i != $this->currPage) {

				$html .= '<li class="uk-pnum"><a href="'.$url.$i.'">'.$i.'</a></li>';
			} else {
				$html .= '<li class="uk-pnum uk-active"><a href="'.$url.$i.'">'.$i.'</a></li>';
			}
		}

		if ($this->hasNext())
			$html .= '<li><a href="'.$url.($this->currPage+1).'"><span uk-pagination-next></span></a></li>';
		else
			$html .= '<li class="uk-disabled"><a href="'.$url.($this->currPage+1).'"><span uk-pagination-next></span></a></li>';

		$html .= '</ul>';

		return $html;
	}
}