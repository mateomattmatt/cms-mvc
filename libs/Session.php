<?php 

class Session {
	public static function init(){
		session_start();
	}

	public static function destroy(){
		session_destroy();
	}

	public static function set($key, $value){
		$_SESSION[$key] = $value;
	}

	public static function get($key){
		return isset($_SESSION[$key]) ? $_SESSION[$key] : false;
	}

	public static function destroyFlash($key){
		if(isset($_SESSION[$key])){
			unset($_SESSION[$key]);
		}
	}

	public function getSession()
    {
        return $_SESSION;
    }
}