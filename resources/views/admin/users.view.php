<div id="wrapper">
  <?php include 'resources/components/sidebar.view.php'; ?>
  <div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
      <?php include 'resources/components/topbar.view.php'; ?>
      <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold">User Management</h6>
                        <div>
                            <button class="btn btn-sm btn-primary"  data-toggle="modal" data-target="#new-user">Add new user</button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Email</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for($i = 0; $i < count($this->users); $i++): $user = $this->users[$i];?>
                                <tr>
                                    <td><?=$user->fname.' '.$user->lname?></td>
                                    <td><?=$user->email?></td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End of home -->
            </div>
        </div>
      </div>
      
    </div>
  </div>
</div>

<div class="modal" tabindex="-1" role="dialog" id="new-user">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Add new User</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="/admin/userstore" method="POST">
          <div class="modal-body container">
          <div class="form-material">
            <label for="fname">First name</label>
            <input type="text" name="fname" id="fname" required />
          </div>
          <div class="form-material">
            <label for="lname">Last name</label>
            <input type="text" name="lname" id="lname" required />
          </div>
          <div class="form-material">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" required />
          </div>
          <div class="form-material">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" required />
          </div>
          <div class="form-material">
            <label for="cpassword">Confirm password</label>
            <input type="password" name="cpassword" id="cpassword" required />
          </div>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-primary">Submit</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal" id="modal-close">Close</button>
          </div>
      </form>
    </div>
  </div>
</div>


