<?php 

class LeadsController extends Controller{
	public function __construct(){
		parent::__construct();
	}

	public function add(){
        $user_id = DB_Table::insert('leads', [
            "name" 	=> $_POST['name'],
            "phone" 	=> $_POST['phone'],
            "email" 	=> $_POST['email'],
            "message" 	=> $_POST['message']
        ]);
        $this->view->redirect('');
	}
}