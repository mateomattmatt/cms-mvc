<?php 

include_once('../libs/Database.php');
include_once('../utils/config.php');


Database::createTable("users", "
  CREATE TABLE IF NOT EXISTS users(
	id			INT(6) 			AUTO_INCREMENT,
	fname	 	VARCHAR(35) 	NOT NULL,
	lname 		VARCHAR(35) 	NOT NULL,
	email 		VARCHAR(50) 	NOT NULL,
  	password	VARCHAR(255) 	NOT NULL,
	image 		VARCHAR(255)     NULL,

	created_at	TIMESTAMP,
	deleted_at	DATETIME		DEFAULT NULL,
	
	PRIMARY KEY(id)
  )
");

Database::createTable("images", "
  CREATE TABLE IF NOT EXISTS images(
	id 			INT(6) 			AUTO_INCREMENT,
	name		VARCHAR(255) 	NOT NULL,
	path		VARCHAR(255) 	NOT NULL,

	created_at	TIMESTAMP,
	deleted_at 	DATETIME 		DEFAULT NULL,
	
	PRIMARY KEY(id)
  )
");

Database::createTable("items", "
  CREATE TABLE IF NOT EXISTS items(
	id			INT(6)			AUTO_INCREMENT,
	code		VARCHAR(255) 	NOT NULL,
	value		TEXT	NOT NULL,

	created_at	TIMESTAMP,
	deleted_at	DATETIME		DEFAULT NULL,

	PRIMARY KEY(id)
  )
");

Database::createTable('logs', "
  CREATE TABLE IF NOT EXISTS logs(
	id 			INT(6)			AUTO_INCREMENT,
	user		VARCHAR(255)	NULL,
	log			VARCHAR(255)	NULL,
	ip			VARCHAR(255)	NULL,
	agent		VARCHAR(255)	NULL,
	address		VARCHAR(255)	NULL,
	
	created_at	TIMESTAMP,
	deleted_at	DATETIME		DEFAULT NULL,

	PRIMARY KEY(id)
  )
");

Database::createTable("leads", "
	CREATE TABLE IF NOT EXISTS leads(
		id		INT(6)			AUTO_INCREMENT,
		name	VARCHAR(255)	NULL,
		phone	VARCHAR(255) 	NULL,
		email	VARCHAR(255)	NULL,
		message TEXT			NULL,

		created_at	TIMESTAMP,
		deleted_at	DATETIME		DEFAULT NULL,

		PRIMARY KEY(id)
	)
");