<?php


$home = [
    'home_hero',
    'about_title',
    'about_body',
    'gallery_title',
    'gallery_img',
    'contact_title',
    'contact_body',
    'contact_map'
];