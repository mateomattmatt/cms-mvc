<div class="row">
  <div class="col-12 col-md-8 col-lg-6 offset-lg-3 offset-md-2">
    <div class="login">
      <div class="login-block">
        <div class="login-block__title">Register Account</div>
        <div class="login-block__subtitle">Create your superadmin account.</div>
        <?php if (!empty(Session::get('error'))): ?>
          <div class="alert alert-danger">
            <?= Session::get('error') ?> 
          </div>
        <?php endif; ?>
        <form action="/initial/register" method="POST">
          <div class="form-material">
            <label for="fname">First name</label>
            <input type="text" name="fname" id="fname" required />
          </div>
          <div class="form-material">
            <label for="lname">Last name</label>
            <input type="text" name="lname" id="lname" required />
          </div>
          <div class="form-material">
            <label for="email">Email</label>
            <input type="email" name="email" id="email" required />
          </div>
          <div class="form-material">
            <label for="password">Password</label>
            <input type="password" name="password" id="password" required />
          </div>
          <div class="form-material">
            <label for="cpassword">Confirm password</label>
            <input type="password" name="cpassword" id="cpassword" required />
          </div>
          <div class="form-material text-right">
            <button type="submit" class="mt-3">Continue to dashboard</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
