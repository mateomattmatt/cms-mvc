<?php 

class SectionController extends Controller {
  public function __construct () {
    parent::__construct();
    parent::middleware('initial|auth');
  }

  public function add () {
    if ($this->view->post()) {
      $section_id = DB_Table::insert('sections', [
        'name' => $_POST['name']
      ]);
      if ($section_id) {
        $this->view->redirect('/admin/home');
      }
    } else {
      $this->view->error();
    }
  }

  public function item () {
    if ($this->view->post()) {
      $items = DB_Table::where('section_items', [
        'section_id' => $_POST['section_id'], 
        'item_key' => $_POST['item_key']
      ]);
      if (!empty($items)) {
        $section_items = DB_Table::where('section_items', [
          'item_key' => $_POST['item_key'], 
          'section_id' => $_POST['section_id']
        ]);
        if (!empty($section_items)) {
          $item = $section_items[0];
          $item_update = DB_Table::update("section_items", [
            'item_value' => $_POST['item_value']
          ], ["id" => $item->id]);
        }
      } else {
        $item_id = DB_Table::insert('section_items', [
          'section_id' => $_POST['section_id'],
          'item_label' => $_POST['item_label'],
          'item_key' => $_POST['item_key'],
          'item_value' => $_POST['item_value']
        ]);

      }
      $this->view->redirect('admin/home');
    } else {
      $this->view->error();
    }
  }

  public function items () {
    if ($this->view->post()) {
      echo json_encode($_POST['items']);
      // $findings = DB_Table::where('section_items', [
      //   'item_key' => $_POST['item_key'],
      //   'section_id' => $_POST['section_id']
      // ]);
      // $images = join("|", $_POST['image']);
      // if (!empty($findings)) {
      //   $item = $findings[0];
      //   $item_update = DB_Table::update("section_items", [
      //     'item_value' => $images
      //   ], ["id" => $item->id]);
      // } else {
      //   $items_id = DB_Table::insert('section_items', [
      //     'section_id' => $_POST['section_id'],
      //     'item_label' => $_POST['item_label'],
      //     'item_key' => $_POST['item_key'],
      //     'item_value' => $images
      //   ]);
      // }
      // $this->view->redirect('admin/home');
    } else {
      $this->view->error();
    }
  }


}