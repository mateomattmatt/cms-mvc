<div class="page-wrapper">
  <div class="login">
    <div class="login-block">
      <div class="login-block__title">Welcome Back!</div>
      <div class="login-block__subtitle">Sign in using your email.</div>
      <?php if (!empty(Session::get('error'))): ?>
        <div class="alert alert-danger">
          <?= Session::get('error') ?> 
        </div>
      <?php endif; ?>
      <form action="/auth/login" method="POST">
        <div class="form-material">
          <label>Email Address</label>
          <input type="email" name="email" />
        </div>
        <div class="form-material">
          <label for="password">Password</label>
          <input id="password" type="password" name="password" />
        </div>
        <div class="form-material text-right">
          <button type="submit" class="mt-3">Sign in</button>
        </div>
      </form>
    </div>
  </div>
</div>
<?php Session::destroyFlash('error'); ?>