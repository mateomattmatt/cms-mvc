<?php
class ImagesController extends Controller{
	public function __construct(){
		parent::__construct();
    }
    
	public function add() {
        if ($_FILES['image']['name'] != "") {
            $name = time();
            $image_id = DB_Table::insert('images', [
                "name"          => $name,
                "path"          => ""
            ]);

            $pathname = $_FILES['image']['name'];
            $target_dir = "public/upload/";
            $target_file = $target_dir . basename($_FILES["image"]["name"]);

            unlink($target_dir.$image_test->image);
            $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
            $extensions_arr = array("jpg","jpeg","png","gif");
            $extensionName = $imageFileType;

            if (in_array($imageFileType,$extensions_arr)) {
                move_uploaded_file($_FILES['image']['tmp_name'],$target_dir.$pathname);
            }
            rename($target_dir.$pathname, $target_dir.$name.".".$extensionName);	

            $image = DB_Table::update('images', [
                "path" 	=> '/'.$target_dir.$name.".".$extensionName,
            ], ["id" => $image_id]);
        }
        $this->view->redirect('admin/home#mediaGallery');
	}
}