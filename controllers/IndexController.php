<?php 

class IndexController extends Controller{
	public function __construct(){
		parent::__construct();
		parent::middleware('initial');
	}

	public function index(){
		$home = [
		  'home_hero',
		  'about_title',
		  'about_body',
		  'gallery_title',
		  'gallery_img',
		  'contact_title',
		  'contact_body',
		  'contact_map'
		];
	
	
		for ($i = 0; $i < count($home); $i++) {
		  $item = DB_Table::where('items', ["code" => $home[$i]])[0];
		  switch ($item->code) {
			case 'home_hero':
			  $this->view->home_hero = $item->value;
			  break;
			case 'about_title':
			  $this->view->about_title = $item->value;
			  break;
			case 'about_body':
			  $this->view->about_body = $item->value;
			  break;
			case 'gallery_title':
			  $this->view->gallery_title = $item->value;
			  break;
			case 'gallery_img':
			  $this->view->gallery_img = json_decode($item->value);
			  break;
			case 'contact_title':
			  $this->view->contact_title = $item->value;
			  break;
			case 'contact_body':
			  $this->view->contact_body = $item->value;
			  break;
			case 'contact_map':
			  $this->view->contact_map = $item->value;
			  break;
		  }
		}
		$this->view->render('index');
	}
}