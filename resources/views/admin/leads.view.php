<div id="wrapper">
  <?php include 'resources/components/sidebar.view.php'; ?>
  <div id="content-wrapper" class="d-flex flex-column">
    <div id="content">
      <?php include 'resources/components/topbar.view.php'; ?>

      <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                <div class="card shadow mb-4">
                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h6 class="m-0 font-weight-bold">Leads</h6>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Phone</th>
                                    <th>Email</th>
                                    <th>Message</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php for($i = 0; $i < count($this->leads); $i++): $lead = $this->leads[$i];?>
                                <tr>
                                    <td><?=$lead->name?></td>
                                    <td><?=$lead->phone?></td>
                                    <td><?=$lead->email?></td>
                                    <td><?=$lead->message?></td>
                                </tr>
                                <?php endfor; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- End of home -->
            </div>
        </div>
      </div>
      
    </div>
  </div>
</div>


