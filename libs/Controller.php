<?php 

class Controller{
  public function __construct () {
    $this->view = new View();
  }

  public function base_model ($name) {
    $path = 'models/'.$name.'Model.php';
    if (file_exists($path)) {
      require $path;
      $modelName = $name.'Model';
      $this->model = new $modelName();
    }

  }

  public function middleware ($types) {
    $arr = explode("|", $types);

    if (in_array('guest', $arr)) {
      Session::init();
      if (Session::get('user')) {
        header("Location: ".SITE_URL."admin/home");
      }
    }
    if (in_array('auth', $arr)) {
      Session::init();
      if (!Session::get('user')) {
        header("Location: ".SITE_URL);
      }
    }
    if (in_array('initial', $arr)) {
      if (empty(DB_Table::all('users'))) {
        header("Location: ".SITE_URL.'initial');
      }
    }
    if (in_array('check-super-admin', $arr)) {
      if (!empty(DB_Table::all('users'))) {
        header("Location: ".SITE_URL);
      }
    }
  }
}